module gitlab.com/etke.cc/tools/ansible-ssh

go 1.21

require (
	github.com/adrg/xdg v0.4.0
	gitlab.com/etke.cc/go/ansible v0.0.0-20240226184118-cb62952794a4
	gopkg.in/yaml.v3 v3.0.1
)

require (
	golang.org/x/exp v0.0.0-20240416160154-fe59bbe5cc7f // indirect
	golang.org/x/sys v0.19.0 // indirect
)
